package com.erf;

import com.erf.codec.ServerClassCodec;
import com.erf.message.SubmitRequest;
import com.erf.tools.SpringContextHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Method;

/**
 * @author zhangzy
 * @date 2022/7/5
 */
@SpringBootTest
public class RpcApplicationTest {
    @Test
    void test(){
//        Gson gson  = new GsonBuilder().registerTypeAdapter(Class.class,new ServerClassCodec.ClassTypeAdapter()).create();
//        String s = gson.toJson(new SubmitRequest());
//        System.out.println(s);
//        SubmitRequest submitRequest = gson.fromJson(s, SubmitRequest.class);
//        try {
//            Class<?> aClass = Class.forName("java.lang.String");
//            System.out.println(aClass);
//
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println(submitRequest);
    }
    @Test
    void testBean(){
        try {
            Class<?> aClass = Class.forName("com.erf.tools.Test");
            System.out.println(aClass);
            Method call = aClass.getMethod("call", String.class);
            Object hello = call.invoke(aClass.newInstance(),"hello");
            System.out.println(hello);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Autowired
    SpringContextHelper springContextHelper;

    @Test
    void testBean2(){
        try {
            Class<?> aClass = Class.forName("com.erf.tools.Test");
            System.out.println(aClass);
            Method call = aClass.getMethod("call", String.class);

            springContextHelper.getBean("test");

            Object hello = call.invoke(new Object(),"hello");
            System.out.println(hello);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
