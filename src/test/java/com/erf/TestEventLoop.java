package com.erf;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class TestEventLoop {

    public static void main(String[] args) {
        //io时间 普通任务 定时任务
        EventLoopGroup group = new NioEventLoopGroup();
        group.next().submit(() -> {
            log.info("hello");
        });
        //定时执行
        group.next().schedule(() -> {
            log.info("schedule");
        }, 10, TimeUnit.SECONDS);
        //定时task
        group.next().scheduleAtFixedRate(() -> {
            log.info("scheduleAtFixedRate");
        },0, 1, TimeUnit.SECONDS);
    }
}
