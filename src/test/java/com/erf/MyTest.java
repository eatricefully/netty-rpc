package com.erf;

import com.erf.tools.ByteUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.EmptyByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.concurrent.*;

public class MyTest {
    @Test
    public void testCallable() throws Exception {
        Callable<String> callable = () -> {
            Thread.sleep(5000);
            return "abc";
        };
        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 0, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        Future<String> submit = executor.submit(callable);
        String s = submit.get(1,TimeUnit.SECONDS);
        System.out.println(s);
    }
    @Test
    public void test01(){
        String s = "ce测试";
        byte[] bytes = s.getBytes(StandardCharsets.UTF_8);
        System.out.println(s.length());
        System.out.println(bytes.length);
    }

    @Test
    public void test02(){
        String s = "ce测试";
        byte[] bytes = s.getBytes(StandardCharsets.UTF_8);
        ByteBuf byteBuf = PooledByteBufAllocator.DEFAULT.buffer();
        byteBuf.writeBytes(bytes);

        CharSequence charSequence = byteBuf.readCharSequence(8, StandardCharsets.UTF_8);
        System.out.println(charSequence);
    }
    @Test
    public void test03(){
        Class<?>[] classes = {String.class};
        byte[] bytes = ByteUtil.encodeObjectToBytes(classes);
        System.out.println(Arrays.toString(bytes));
        Object o = ByteUtil.decodeBytesToObject(bytes);
        Object[] objects = (Object[])o;
        System.out.println(objects[0]);
    }

}
