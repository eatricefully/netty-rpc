package com.erf.server;

import com.erf.initalizer.ServerChildChannelInitializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 单例模式
 * @author zhangzy
 * @date 2022/7/5
 */
public class RpcServer {

    private final ServerBootstrap serverBootstrap;

    private RpcServer() {
        NioEventLoopGroup boss = new NioEventLoopGroup();
        NioEventLoopGroup worker = new NioEventLoopGroup();
        serverBootstrap = new ServerBootstrap()
                .group(boss, worker)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ServerChildChannelInitializer());
    }

    public void bind(int port) {
        serverBootstrap.bind(port);
    }

    private static class SingletonHolder {
        private static final RpcServer INSTANCE = new RpcServer();
    }

    public static RpcServer getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
