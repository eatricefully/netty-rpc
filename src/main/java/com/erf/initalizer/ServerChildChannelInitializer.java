package com.erf.initalizer;

import com.erf.codec.ByteBufToMessageCodec;
import com.erf.handler.server.*;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * @author zhangzy
 * @date 2022/7/7
 */
public class ServerChildChannelInitializer extends ChannelInitializer<NioSocketChannel> {
    @Override
    protected void initChannel(NioSocketChannel channel) throws Exception {
        channel.pipeline().addLast(new LengthFieldBasedFrameDecoder(1024 * 1024, 0, 4, -4, 0, true));
        channel.pipeline().addLast(new ByteBufToMessageCodec());
        channel.pipeline().addLast(new BaseServerChannelDuplexHandler());
        channel.pipeline().addLast(new LoginRequestHandler());
        channel.pipeline().addLast(new IdleStateHandler(10,10,10));
        channel.pipeline().addLast(new HeartBeatServerHandler(3));
        channel.pipeline().addLast(new BusinessHandler());
        channel.pipeline().addLast(new BlackHoleHandler());
    }
}
