package com.erf.initalizer;

import com.erf.codec.ByteBufToMessageCodec;
import com.erf.handler.client.BaseClientChannelDuplexHandler;
import com.erf.handler.client.SubmitRequestResponseHandler;
import com.erf.handler.server.*;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * @author zhangzy
 * @date 2022/7/8
 */
public class ClientChannelInitializer extends ChannelInitializer<NioSocketChannel> {
    @Override
    protected void initChannel(NioSocketChannel channel) throws Exception {
        channel.pipeline().addLast(new LengthFieldBasedFrameDecoder(1024 * 1024, 0, 4, -4, 0, true));
        channel.pipeline().addLast(new ByteBufToMessageCodec());
        channel.pipeline().addLast(new BaseClientChannelDuplexHandler());
        channel.pipeline().addLast(new SubmitRequestResponseHandler());
        channel.pipeline().addLast(new BlackHoleHandler());
    }
}
