package com.erf.config;

import com.erf.client.RpcClient;
import com.erf.server.RpcServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhangzy
 * @date 2022/7/7
 */
@Configuration
public class SpringRpcConfig {
    @Bean
    public RpcServer rpcServer() {
        RpcServer instance = RpcServer.getInstance();
        instance.bind(7080);
        return instance;
    }

    @Bean
    public RpcClient rpcClient() {
        RpcClient instance = RpcClient.getInstance();
        instance.connect("zhangzy", "localhost", 7080);
        return instance;
    }

}
