package com.erf.exception;

/**
 * @author zhangzy
 * @date 2022/7/9
 */
public class NoConnectException extends Exception {
    public NoConnectException(String userName) {
        super("未找到归属于userName:[" + userName + "]的连接");
    }
}
