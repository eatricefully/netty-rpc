package com.erf.message;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangzy
 * @date 2022/7/5
 * @history 非public不被子类继承的属性将不会被gson序列化, 使用abstract类gson找不到无参构造器。
 * @history 2022/7/10转化为ByteBuf解析，取缔gson+字符串。改为abstract类便于控制+解决网络拆包。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseMessage {

    private int length;
    private int command;
    private int sequence;

    /**
     * 将本对象编码为byte数组
     *
     * @return byte编码
     * @param byteBuf
     */
    public abstract void encode(ByteBuf byteBuf);

    /**
     * 将byte数组转化为本对象
     *
     * @param length,command,sequence 报文头
     * @param body                    解码对象
     * @return 继承BaseMessage的对象
     */
    public abstract BaseMessage decode(int length, int command, int sequence, ByteBuf body);
}
