package com.erf.message;

import com.erf.tools.ByteUtil;
import com.erf.tools.SequenceUtil;
import io.netty.buffer.ByteBuf;
import lombok.ToString;

/**
 * @author zhangzy
 * @date 2022/7/5
 */
@ToString
public class ActiveTestResponse extends BaseMessage {
    @Override
    public void encode(ByteBuf byteBuf) {
        byteBuf.writeInt(12);
        byteBuf.writeInt(0x80000003);
        byteBuf.writeInt(SequenceUtil.getSequenceId());
    }

    @Override
    public BaseMessage decode(int length, int command, int sequence, ByteBuf body) {
        this.setLength(length);
        this.setCommand(command);
        this.setSequence(sequence);
        return this;
    }

}
