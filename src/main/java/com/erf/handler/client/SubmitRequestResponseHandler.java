package com.erf.handler.client;

import com.erf.message.SubmitRequestResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.concurrent.Promise;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zhangzy
 * @date 2022/7/9
 */
@Slf4j
public class SubmitRequestResponseHandler extends SimpleChannelInboundHandler<SubmitRequestResponse> {
    public static final Map<Integer, Promise<SubmitRequestResponse>> PROMISES = new ConcurrentHashMap<>();

    @Override
    public void channelRead0(ChannelHandlerContext ctx, SubmitRequestResponse msg) throws Exception {
        log.info("[client_receive]" + msg);
        int sequence = msg.getSequence();
        Promise<SubmitRequestResponse> promise = PROMISES.remove(sequence);
        if (promise != null) {
            promise.setSuccess(msg);
        } else {
            log.error("找不到seq:{}的消息体，submitRequest/submitRequestResponse对应失败！", sequence);
        }
    }

}
