package com.erf.handler.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zhangzy
 * @date 2022/7/5
 */
@Slf4j
@ChannelHandler.Sharable
public class BlackHoleHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //底层已有ByteBuf的判断，无脑释放即可
        ReferenceCountUtil.release(msg);
        log.debug(msg.toString());
    }
}
