package com.erf.tools;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zhangzy
 * @date 2022/7/10
 */
public class SequenceUtil {
   private static AtomicInteger SEQUENCE_ID = new AtomicInteger(0);

    public static int getSequenceId() {
        return SEQUENCE_ID.getAndIncrement();
    }
}
