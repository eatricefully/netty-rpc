package com.erf.tools;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author zhangzy
 * @date 2022/7/5
 */
@Component
@Slf4j
public class Test {
    public String call(String str) {
        log.info("-------------" + str + "----------");
        return "returnStr";
    }

    public void throwAnException(String str) throws Exception{
        log.info("-----exception is coming--------" + str + "----------");
        throw new Exception("exception!!!");
    }
}
