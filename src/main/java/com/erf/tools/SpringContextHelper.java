package com.erf.tools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author zhangzy
 * @date 2022/7/5
 */
@Component
public class SpringContextHelper extends ApplicationObjectSupport {

    /**
     * 提供一个接口，获取容器中的Bean实例，根据名称获取
     */
    public Object getBean(String beanName) {
        return Objects.requireNonNull(getApplicationContext()).getBean(beanName);
    }

}
