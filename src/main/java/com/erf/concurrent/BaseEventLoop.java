package com.erf.concurrent;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
/**
 * @author zhangzy
 * @date 2022/7/9
 */
public class BaseEventLoop {
    public static EventLoopGroup group = new NioEventLoopGroup(2);
}
