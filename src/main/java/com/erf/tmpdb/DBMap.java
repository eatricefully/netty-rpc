package com.erf.tmpdb;

import com.erf.message.*;

import java.util.HashMap;
/**
 * @author zhangzy
 * @date 2022/7/5
 */
public class DBMap {
    public static HashMap<String, String> loginMap = new HashMap<>();
    public static HashMap<Integer, Class<?>> commandMap = new HashMap<>();
    public static HashMap<Class<?>, Integer> classMap = new HashMap<>();

    static {
        loginMap.put("zhangzy", "980980");
        loginMap.put("zhuhc", "980980");
        loginMap.put("zhanggx", "980980");
        commandMap.put(0x00000001, LoginRequest.class);
        commandMap.put(0x80000001, LoginRequestResponse.class);
        commandMap.put(0x00000002, SubmitRequest.class);
        commandMap.put(0x80000002, SubmitRequestResponse.class);
        commandMap.put(0x00000003, ActiveTest.class);
        commandMap.put(0x80000003, ActiveTestResponse.class);

        classMap.put(LoginRequest.class, 0x00000001);
        classMap.put(LoginRequestResponse.class, 0x80000001);
        classMap.put(SubmitRequest.class, 0x00000002);
        classMap.put(SubmitRequestResponse.class, 0x80000002);
        classMap.put(ActiveTest.class, 0x00000003);
        classMap.put(ActiveTestResponse.class, 0x80000003);
    }
}
