#### 基于netty开发rpc远程调用框架

> 主分支已被替换为ByteBuf解析，简单String解析见stringCodec分支。

##### 基于Spring的简单调用方式举例

- Server

  > 开启server端，用于接受client端发送的Submit包，并将Submit包解析成调用方法去调用。

  ```java
  package com.erf.config;
  
  import com.erf.server.RpcServer;
  import org.springframework.context.annotation.Bean;
  import org.springframework.context.annotation.Configuration;
  
  @Configuration
  public class SpringRpcConfig {
      @Bean
      public RpcServer rpcServer() {
          RpcServer instance = RpcServer.getInstance();
          instance.bind(7080);
          return instance;
      }
  }
  ```

- Client

  > Client端用于发送submit包，主动让server端调用自己的方法。client可以和server不在一台机器。

  ```java
  package com.erf.config;
  
  import com.erf.client.RpcClient;
  import org.springframework.context.annotation.Bean;
  import org.springframework.context.annotation.Configuration;
  
  @Configuration
  public class SpringRpcConfig {
      @Bean
      public RpcClient rpcClient() {
          RpcClient instance = RpcClient.getInstance();
          instance.connect("zhangzy", "localhost", 7080);
          return instance;
      }
  
  }
  ```

  

